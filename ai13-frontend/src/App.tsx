import './App.css'
import '@fontsource/inter';
import {CssBaseline} from "@mui/joy";
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import {useContext, useEffect} from "react";
import {AuthContext} from "./AuthContext.tsx";
import Login from "./pages/Login.tsx";
import VisitorQuizzes from "./pages/VisitorQuizzes.tsx";
import Signup from "./pages/Signup.tsx";
import InternQuizzes from "./pages/InternQuizzes.tsx";
import InternQuiz from "./pages/InternQuiz.tsx";
import Layout from "./components/Layout.tsx";
import InternStatistics from "./pages/InternStatistics.tsx";
import Statistics from "./pages/Statistics.tsx";

function App() {

    const {jwt, checkIsAuthenticated} = useContext(AuthContext);

    useEffect(() => {
        checkIsAuthenticated()
    }, [])

    return (
        <BrowserRouter>
            <CssBaseline>
                {!jwt && (
                    <Routes>
                        <Route element={<Layout/>}>
                            <Route path={"/visitor-quizzes"} element={<VisitorQuizzes/>}/>
                            <Route path={"/statistics"} element={<Statistics/>}/>
                            <Route path={"/login"} element={<Login/>}/>
                            <Route path={"/signup"} element={<Signup/>}/>
                            <Route path={"*"} element={<Navigate to={"/visitor-quizzes"}/>}/> {/*redirect*/}
                        </Route>

                    </Routes>
                )}
                {jwt && (
                    <Routes>
                        <Route element={<Layout/>}>
                            <Route path={"/intern-quizzes"} element={<InternQuizzes/>}/>
                            <Route path={"/intern-quizzes/:quizId"} element={<InternQuiz/>}/>
                            <Route path={"/intern-statistics"} element={<InternStatistics/>}/>
                            <Route path={"/statistics"} element={<Statistics/>}/>
                            <Route path={"*"} element={<Navigate to={"/intern-quizzes"}/>}/> {/*redirect*/}
                        </Route>
                    </Routes>
                )}
            </CssBaseline>
        </BrowserRouter>
    )
}

export default App
