interface Subject {
    id: number,
    name: string,
    order: number,
    isActive: boolean
}

interface PartialInfosOfQuiz {
    id: number,
    name: string,
    description: string,
    numberOfQuestions: number
}

interface Question {
    id: number,
    label: string,
    answers: Answer[]
}

interface Answer {
    id: number,
    label: string
}

type RankingEntry = { 
    userId: number, 
    score: number 
};

type UserDTO = {
    id: number;
    name: string;
    email: string
};

type UserStats = {
    averageScore: number;
    averageDuration: number
};
