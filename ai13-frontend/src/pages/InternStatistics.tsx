import {AuthContext} from "../AuthContext.tsx";
import {useContext, useEffect, useState} from "react";
import {Chip, Sheet, Stack, Typography} from "@mui/joy";

function InternStatistics() {
    const {jwt, checkIsAuthenticated} = useContext(AuthContext);

    const [currentUserJourneys, setCurrentUserJourneys] = useState<[] | undefined>(undefined);

    function retrieveCurrentUserJourneys() {
        fetch("/api/v1/journeys/current-user", {
            headers: {
                "Authorization": "Bearer " + jwt
            }
        })
            .then(response => {
                if (response.ok) {
                    return response.json().then(json => setCurrentUserJourneys(json))
                } else {
                    checkIsAuthenticated()
                }
            })
    }

    useEffect(() => {
        retrieveCurrentUserJourneys()
    }, []);

    if (currentUserJourneys) {
        return (
            <Stack spacing={2}>
                {currentUserJourneys.map(journey => {
                    return (
                        <Sheet variant={"outlined"} sx={{padding: 1}} key={journey.id}>
                            <Typography>{journey.quiz.name}</Typography>
                            <Chip size={"sm"} color={"primary"} variant={"soft"}>{journey.quiz.subject.name}</Chip>
                            <Typography>Score : {journey.score}/{journey.answers.length}</Typography>
                            <Typography>Duration : {journey.duration} seconds</Typography>
                        </Sheet>
                    )
                })}
            </Stack>
        );
    }

}

export default InternStatistics;