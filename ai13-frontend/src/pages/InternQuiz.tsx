import {useParams} from "react-router-dom";
import {useContext, useEffect, useRef, useState} from "react";
import {AuthContext} from "../AuthContext.tsx";
import {Button, CircularProgress, Radio, RadioGroup, Stack, Typography} from "@mui/joy";

function InternQuiz() {
    const { quizId } = useParams();

    const {jwt, checkIsAuthenticated} = useContext(AuthContext);
    const [questions, setQuestions] = useState<Question[] | undefined>(undefined);

    const [currentQuestionIndex, setCurrentQuestionIndex] = useState<number | undefined>(undefined);
    const [selectedAnswerId, setSelectedAnswerId] = useState<number | null>(null);

    const [isQuizFinished, setIsQuizFinished] = useState<boolean>(false);

    const [isSendingAnswers, setIsSendingAnswers] = useState<boolean>(false);
    const [hasSentAnswersSuccessfully, setHasSentAnswersSuccessfully] = useState<boolean>(false);

    const selectedAnswersId = useRef<number[]>([]);
    const startDate = useRef(Date.now());

    function retrieveQuestions() {
        fetch("/api/v1/quizzes/" + quizId + "/questions", {
            headers: {
                "Authorization": "Bearer " + jwt
            }
        })
            .then(response => {
                if (response.ok) {
                    return response.json()
                        .then(questions => {
                            setQuestions(questions)
                            setCurrentQuestionIndex(0)
                        })
            } else {
                    checkIsAuthenticated()
            }
        })
    }

    useEffect(() => {
        retrieveQuestions()
    }, []);

    useEffect(() => {
            setSelectedAnswerId(null)
    }, [currentQuestionIndex]);

    function handleValidateButtonClick() {
        setCurrentQuestionIndex(prevIndex => {
            if (prevIndex === undefined || !questions || selectedAnswerId === null) {
                return undefined;
            }

            selectedAnswersId.current.push(selectedAnswerId)

            // if questions are left
            if (prevIndex < questions.length - 1) {
                return prevIndex + 1;
            } else {
                setIsQuizFinished(true)
                sendAnswers()
                return undefined;
            }
        })
    }

    function sendAnswers() {
        const body = {
            quizId: quizId,
            duration: Math.floor((Date.now() - startDate.current) / 1000),
            answersId: selectedAnswersId.current
        }
        setIsSendingAnswers(true)

        fetch("/api/v1/journeys", {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + jwt,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }).then(r => {
            setIsSendingAnswers(false)
            if (r.ok) {
                setHasSentAnswersSuccessfully(true)
            } else {
                checkIsAuthenticated()
            }
        })
    }

    if (isQuizFinished && isSendingAnswers) {
        return (
            <Stack alignItems={"center"} spacing={2}>
                <Typography>Wait for your answers to be sent</Typography>
                <CircularProgress/>
            </Stack>
        )
    }

    if (isQuizFinished && hasSentAnswersSuccessfully) {
        return (
            <Stack>
                <Typography>Your answers has been sent. You can safely close the quiz.</Typography>
            </Stack>

        )
    }

    if (questions && currentQuestionIndex !== undefined) {
        const currentQuestion = questions[currentQuestionIndex]

        const currentQuestionAnswers = questions[currentQuestionIndex].answers

        return <Stack spacing={2} alignItems={"center"}>
            <Typography>{currentQuestion.label}</Typography>
            <RadioGroup value={selectedAnswerId} onChange={event => setSelectedAnswerId(parseInt(event.target.value))}>
                {currentQuestionAnswers.map(answer => {
                    return <Radio key={answer.id} value={answer.id} label={answer.label} />
                })}
            </RadioGroup>
            <Button disabled={selectedAnswerId === null} onClick={handleValidateButtonClick}>
                Validate response
            </Button>
        </Stack>;
    }
}
export default InternQuiz;