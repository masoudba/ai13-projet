import {Card, Chip, Option, Select, Stack, Input, Button} from "@mui/joy";
import { List, ListItem, ListItemText } from '@mui/material';
import {useEffect, useState} from "react";

function VisitorQuizzes() {
    const [subjects, setSubjects] = useState<Subject[] | undefined>(undefined);
    const [selectedSubject, setSelectedSubject] = useState<Subject | undefined>(undefined);

    const [partialInfosOfQuizzes, setPartialInfosOfQuizzes] = useState<PartialInfosOfQuiz[] | undefined | null>(undefined);

    useEffect(() => {
        fetchSubjects()
    }, []);

    useEffect(() => {
        fetchPartialInfosOfQuizzes()
    }, [selectedSubject]);

    function fetchSubjects() {
        fetch("/api/v1/subjects")
            .then(response => response.json())
            .then(json => setSubjects(json));
    }

    function fetchPartialInfosOfQuizzes() {
        if (selectedSubject) {
            fetch("/api/v1/subjects/" + selectedSubject.id + "/quizzes/partial-infos")
                .then(response => response.json())
                .then(json => setPartialInfosOfQuizzes(json));
        }

    }

    function handleSelectSubjectChange(_: never, selectedSubject: Subject) {
        setSelectedSubject(selectedSubject)
    }

    if (subjects) {
        return (
            <Stack alignItems={"center"} spacing={4}>
                {/* Select Dropdown for Subjects */}
                <Select placeholder={"Choose a subject…"}
                        endDecorator={
                            <Chip size="sm" color="primary" variant="soft">{subjects?.length}</Chip>
                        }
                        onChange={handleSelectSubjectChange}>
                    {subjects?.map((subject) => (
                        <Option key={subject.id} value={subject}>{subject.name}</Option>
                    ))}
                </Select>

                {/* Display Partial Information of Quizzes */}
                {partialInfosOfQuizzes &&
                    <Stack spacing={2} alignItems={"center"}>
                        <Chip>
                            {partialInfosOfQuizzes.length} {partialInfosOfQuizzes.length > 1 ? "quizzes" : "quiz"}
                        </Chip>
                        {partialInfosOfQuizzes.map(partialInfosOfQuiz => (
                            <Card key={partialInfosOfQuiz.id}>
                                {partialInfosOfQuiz.name} | {partialInfosOfQuiz.numberOfQuestions} {partialInfosOfQuiz.numberOfQuestions > 1 ? "questions" : "question"}
                                <br />
                                {partialInfosOfQuiz.description}
                            </Card>
                        ))}
                    </Stack>
                }
            </Stack>
        );
    }
}

export default VisitorQuizzes;