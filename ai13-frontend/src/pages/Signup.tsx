import {useState} from 'react'
import {useNavigate} from "react-router-dom";
import {validate} from "email-validator";
import {Alert, Box, Button, Container, FormControl, FormHelperText, FormLabel, Input, Sheet, Stack} from "@mui/joy";

function Signup() {
    const [name, setName] = useState("")
    const [nameError, setNameError] = useState(false)

    const [email, setEmail] = useState("")
    const [emailErrorLabel, setEmailErrorLabel] = useState("")

    const [password, setPassword] = useState("")
    const [passwordErrorLabel, setPasswordErrorLabel] = useState("")

    const [passwordRepeat, setPasswordRepeat] = useState("")
    const [passwordRepeatErrorLabel, setPasswordRepeatErrorLabel] = useState("")

    const [company, setCompany] = useState("")

    const [phoneNumber, setPhoneNumber] = useState("");

    const [signupError, setSignupError] = useState("")

    const [signupButtonLoading, setSignupButtonLoading] = useState(false)

    const navigate = useNavigate();

    function handleSubmit(event: { preventDefault: () => void; }): void {
        event.preventDefault()

        // validate name
        setName(name.trimEnd())
        const nameIsValid = name.trimEnd().length > 0;
        setNameError(!nameIsValid)

        // validate email
        const emailIsValid = validate(email)
        emailIsValid ? setEmailErrorLabel("") : setEmailErrorLabel("E-mail must be valid")

        // validate password
        const passwordIsNotContainingWhitespaces = !password.match(/\s/g);
        if (!passwordIsNotContainingWhitespaces) setPasswordErrorLabel("Password must not contain any whitespaces")
        const passwordIsAtLeast5CharsLong = password.match(/.{5,}/);
        if (!passwordIsAtLeast5CharsLong) setPasswordErrorLabel("Password must be at least 5 characters long")

        const passwordIsValid = passwordIsNotContainingWhitespaces && passwordIsAtLeast5CharsLong;
        if (passwordIsValid) setPasswordErrorLabel("")

        // validate password repeat
        const passwordRepeatIsValid = password === passwordRepeat;
        passwordRepeatIsValid ?
            setPasswordRepeatErrorLabel("") : setPasswordRepeatErrorLabel("Passwords does not match")

        // if form is valid, perform signup
        if (nameIsValid && emailIsValid && passwordIsValid && passwordRepeatIsValid)
            performSignup()
    }

    function performSignup() {
        setSignupButtonLoading(true)
        fetch("/api/v1/users", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password,
                name: name,
                company: company,
                phoneNumber: phoneNumber
            })
        })
            .then(response => {
                setSignupButtonLoading(false)
                if (response.ok) {
                    navigate("/login");
                } else {
                    handleSignupError(response)
                }
            })
    }

    function handleSignupError(errorResponse: Response) {
        if (!errorResponse.ok) {
            return errorResponse.json().then(json => {
                setSignupError(json.message)
            })
        }
    }

    return (
        <Box display="flex" justifyContent="center" alignItems="center" minHeight="70vh">
            <Container maxWidth={"sm"}>
                <Sheet variant={"outlined"} sx={{padding: 3, width: "100%"}}>
                    <form onSubmit={handleSubmit} noValidate={true}>
                        <Stack spacing={2}>
                            <FormControl error={nameError}>
                                <FormLabel>Name</FormLabel>
                                <Input required placeholder={"Enter your name…"} autoFocus
                                       onChange={event => setName(event.target.value)}
                                       value={name}
                                />
                            </FormControl>
                            <FormControl error={!!emailErrorLabel}>
                                <FormLabel>E-mail</FormLabel>
                                <Input required placeholder={"Enter your e-mail…"}
                                       autoComplete={"email"}
                                       onChange={event => setEmail(event.target.value.replace(/\s/g, ''))}
                                       value={email}
                                />
                                <FormHelperText>{emailErrorLabel}</FormHelperText>
                            </FormControl>

                            <FormControl error={!!passwordErrorLabel}>
                                <FormLabel>Password</FormLabel>
                                <Input required placeholder={"Enter your password…"}
                                       onChange={event => setPassword(event.target.value.replace(/\s/g, ''))}
                                       value={password} type={"password"}
                                />
                                <FormHelperText>{passwordErrorLabel}</FormHelperText>
                            </FormControl>

                            <FormControl error={!!passwordRepeatErrorLabel}>
                                <FormLabel>Repeat password</FormLabel>
                                <Input required placeholder={"Repeat your password…"}
                                       onChange={event => setPasswordRepeat(event.target.value.replace(/\s/g, ''))}
                                       value={passwordRepeat} type={"password"}
                                />
                                <FormHelperText>{passwordRepeatErrorLabel}</FormHelperText>
                            </FormControl>

                            <FormControl>
                                <FormLabel>Company</FormLabel>
                                <Input placeholder={"Enter the name of your company…"}
                                       onChange={event => setCompany(event.target.value)}
                                       value={company}
                                />
                            </FormControl>

                            <FormControl>
                                <FormLabel>Phone number</FormLabel>
                                <Input placeholder={"Enter your phone number"}
                                       onChange={event => setPhoneNumber(event.target.value)}
                                       value={phoneNumber} inputMode={"tel"}
                                />
                            </FormControl>

                            <Button type="submit" fullWidth loading={signupButtonLoading} loadingPosition="end">
                                Sign up
                            </Button>

                            {signupError && <Alert color={"danger"}>{signupError}</Alert>}
                        </Stack>
                    </form>
                </Sheet>
            </Container>
        </Box>
    );
}

export default Signup;