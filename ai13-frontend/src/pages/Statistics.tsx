import {Select, Stack, Option, Card, Typography} from "@mui/joy";
import {useEffect, useState} from "react";

function Statistics() {
    const [users, setUsers] = useState<[] | undefined>(undefined);
    const [selectedUserId, setSelectedUserId] = useState<number | undefined>(undefined);

    const [statistics, setStatistics] = useState<object | undefined>(undefined)

    function retrieveUsers() {
        fetch("/api/v1/users")
            .then(response => {
                if (response.ok) {
                    return response.json().then(json => setUsers(json));
                }
            })
    }

    function retrieveStatisticsOfSelectedUser() {
        fetch("/api/v1/users/" + selectedUserId + "/statistics")
            .then(response => {
                if (response.ok) {
                    return response.json().then(json => setStatistics(json))
                }
            })
    }

    useEffect(() => {
        retrieveUsers()
    }, []);

    useEffect(() => {
        retrieveStatisticsOfSelectedUser()
    }, [selectedUserId]);

    if (users) {
        return (
            <Stack spacing={2} alignItems={"center"}>
                <Select placeholder={"Select user…"}
                        onChange={(_, newValue) => setSelectedUserId(newValue)}>
                    {
                        users.map(user => {
                            return (
                                <Option value={user.id}>
                                    {user.name}
                                </Option>
                            )
                        })
                    }
                </Select>
                {
                    statistics &&
                    <Stack spacing={1}>
                        {
                            statistics.map(statistic => {
                                return (
                                    <Card>
                                        <Typography level={"title-lg"} variant={"soft"}>
                                            {statistic.quiz.name}
                                        </Typography>
                                        <Typography level="body-md">
                                            Best score: {statistic.bestScore}
                                        </Typography>
                                        <Typography level="body-md">
                                            Duration: {statistic.duration} seconds
                                        </Typography>
                                    </Card>
                                )
                            })
                        }
                    </Stack>
                }
            </Stack>
        );
    }
}

export default Statistics;