import {useContext, useState} from "react";
import {AuthContext} from "../AuthContext.tsx";
import {
    Box,
    Button,
    Container,
    FormControl,
    FormHelperText,
    FormLabel,
    Input,
    Sheet,
    Stack
} from "@mui/joy";
import {validate} from "email-validator";


function Login() {
    const [email, setEmail] = useState("");
    const [emailErrorLabel, setEmailErrorLabel] = useState("")
    const [password, setPassword] = useState("");
    const [passwordErrorLabel, setPasswordErrorLabel] = useState("")

    const [loginError, setLoginError] = useState(false)
    const [loginButtonLoading, setLoginButtonLoading] = useState(false)

    const {onSuccessfulLogin} = useContext(AuthContext)

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault()

        // validate email
        const emailIsValid = validate(email)
        emailIsValid ? setEmailErrorLabel("") : setEmailErrorLabel("Enter a valid email")
        // validate password
        const passwordIsValid = password.length > 0
        passwordIsValid ? setPasswordErrorLabel("") : setPasswordErrorLabel("Enter your password")

        const formIsValid = (emailIsValid && passwordIsValid)

        if (formIsValid)
            performLogin()
    }

    function performLogin() {
        setLoginButtonLoading(true)
        fetch("api/v1/authentication/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "email": email,
                "password": password
            })
        })
            .then(response => {
                    setLoginButtonLoading(false)
                    if (response.ok) {
                        setLoginError(false)
                        return response.json()
                            .then(json => onSuccessfulLogin(json.accessToken))
                    } else {
                        setLoginError(true)
                        setTimeout(() => setLoginError(false), 3000)
                    }
                }
            )
    }

    return (
        <Box display="flex" alignItems="center" minHeight="70vh">
            <Container maxWidth={"sm"}>
                <Sheet variant={"outlined"} sx={{padding: 3, width: "100%"}}>
                    <form onSubmit={handleSubmit} noValidate={true}>
                        <Stack spacing={2} alignItems={"center"}>
                            <FormControl error={!!emailErrorLabel}>
                                <FormLabel>E-mail</FormLabel>
                                <Input required placeholder={"Enter your e-mail…"}
                                       autoComplete={"email"} autoFocus
                                       onChange={event => setEmail(event.target.value.replace(/\s/g, ''))}
                                       value={email}
                                />
                                <FormHelperText>{emailErrorLabel}</FormHelperText>
                            </FormControl>

                            <FormControl error={!!passwordErrorLabel}>
                                <FormLabel>Password</FormLabel>
                                <Input required placeholder={"Enter your password…"}
                                       autoComplete={"current-password"}
                                       onChange={event => setPassword(event.target.value.replace(/\s/g, ''))}
                                       value={password} type={"password"}

                                />
                                <FormHelperText>{passwordErrorLabel}</FormHelperText>
                            </FormControl>

                            <Button type="submit" fullWidth color={loginError ? "danger" : "primary"}
                                    loading={loginButtonLoading}>
                                Log in
                            </Button>
                        </Stack>
                    </form>
                </Sheet>
            </Container>
        </Box>
    );
}

export default Login;