import {AuthContext} from "../AuthContext.tsx";
import {useContext, useEffect, useState} from "react";
import {Button, Card, Chip, Link, Stack, Typography} from "@mui/joy";
import { Link as RouterLink } from "react-router-dom";

function InternQuizzes() {
    const {jwt, checkIsAuthenticated} = useContext(AuthContext);

    const [pageOfQuizzes, setPageOfQuizzes] = useState(undefined);
    const [currentPageNumber, setCurrentPageNumber] = useState(0);
    const [totalPagesNumber, setTotalPagesNumber] = useState(undefined);

    function retrievePageOfQuizzes(pageNumber: number, pageSize: number) {
        const headers = new Headers();
        headers.append("Authorization", "Bearer " + jwt)

        fetch("/api/v1/quizzes/page?" + new URLSearchParams({
            pageNumber: pageNumber.toString(),
            pageSize: pageSize.toString(),
        }), {
            headers: headers
        })
            .then(response => {
                if (response.ok) {
                    return response.json()
                        .then(json => {
                            setPageOfQuizzes(json.content)
                            setTotalPagesNumber(json.totalPages)
                        })
                } else {
                    checkIsAuthenticated()
                }
            })
    }

    useEffect(() => {
        retrievePageOfQuizzes(currentPageNumber, 4)
    }, [currentPageNumber]);

    return (
        (pageOfQuizzes && currentPageNumber !== undefined && totalPagesNumber !== undefined) ?
            <Stack spacing={2} alignItems={"center"}>
                {pageOfQuizzes.map(quiz => {
                    return (
                        <Card key={quiz.id}
                              sx={{'&:hover': {boxShadow: 'sm', borderColor: 'neutral.outlinedHoverBorder'}}}>
                            <Stack spacing={1} alignItems={"center"}>
                                <Typography>
                                    <Link
                                        component={RouterLink}
                                        to={"/intern-quizzes/" + quiz.id}
                                        overlay
                                        underline="none"
                                        sx={{color: "text.tertiary"}}
                                    >
                                        {quiz.name}
                                    </Link>
                                </Typography>
                                <Chip size={"sm"} variant={"soft"} color={"primary"}>{quiz.subject.name}</Chip>
                            </Stack>
                            <Typography>{quiz.description}</Typography>

                        </Card>
                    )
                })}
                <Stack direction={"row"} alignItems={"center"} spacing={1}>
                    <Button variant={"plain"} size={"sm"} disabled={currentPageNumber === 0}
                            onClick={() => setCurrentPageNumber(prevState => prevState - 1)}>
                        {"<"}
                    </Button>
                    <Typography>{currentPageNumber + 1}</Typography>
                    <Button variant={"plain"} size={"sm"} disabled={currentPageNumber + 1 === totalPagesNumber}
                            onClick={() => setCurrentPageNumber(prevState => prevState + 1)}>
                        {">"}
                    </Button>
                </Stack>
            </Stack> : null
    );
}

export default InternQuizzes;