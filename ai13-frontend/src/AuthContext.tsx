import React, {createContext, useMemo, useState} from "react";

interface AuthContextValue {
    jwt: string;
    checkIsAuthenticated: () => void;
    onSuccessfulLogin: (jwt: string) => void;
    onSuccessfulLogout: () => void
}

// create context
const AuthContext = createContext<AuthContextValue>(undefined);

function AuthContextProvider(props: Readonly<{ children: React.ReactNode }>) {
    let defaultJwtValue: string;
    if (localStorage.getItem("jwt")) {
        try {
            defaultJwtValue = localStorage.getItem("jwt")
        } catch (e) {
            defaultJwtValue = "";
        }
    }

    const [jwt, setJwt] = useState(defaultJwtValue);

    function checkIsAuthenticated() {
        fetch("/api/v1/users/current", {
            headers: {
                "Authorization": "Bearer " + jwt
            }
        })
            .then(response => {
                if (!response.ok) {
                    setJwt("")
                    localStorage.removeItem("jwt")
                }
            })
    }

    function onSuccessfulLogin(jwt: string): void {
        setJwt(jwt)
        localStorage.setItem("jwt", jwt)
    }

    function onSuccessfulLogout(): void {
        setJwt("")
        localStorage.removeItem("jwt")
    }

    const providerValue: AuthContextValue = useMemo(() => ({
        jwt: jwt,
        onSuccessfulLogin,
        onSuccessfulLogout,
        checkIsAuthenticated: checkIsAuthenticated
    }), [jwt]);

    return (
        <AuthContext.Provider value={providerValue}>
            {props.children}
        </AuthContext.Provider>
    );
}

export {AuthContext, AuthContextProvider};