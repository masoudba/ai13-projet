import Appbar from "./Appbar.tsx";
import {Outlet} from "react-router-dom";
import {Box, Stack} from "@mui/joy";

function Layout() {
    return (
        <Stack alignItems={"center"}>
            <Appbar/>
            <Box padding={"2rem"}>
                <Outlet/>
            </Box>
        </Stack>
    );
}

export default Layout;