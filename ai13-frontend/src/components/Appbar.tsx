import {List, ListItem, ListItemButton, Sheet} from "@mui/joy";
import { Link as RouterLink } from 'react-router-dom';
import {AuthContext} from "../AuthContext.tsx";
import {useContext} from "react";

function Appbar() {
    const {jwt, onSuccessfulLogout} = useContext(AuthContext);

    if (jwt) {
        return (
            <Sheet>
                <List orientation={"horizontal"} variant="outlined"
                      sx={{
                          marginTop: 2,
                          boxShadow: 'sm',
                          borderRadius: 'sm',
                          '--List-radius': '8px',
                          '--List-padding': '4px'
                      }}
                >
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/intern-quizzes"}>
                            Quizzes
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/intern-statistics"}>
                            My Statistics
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/statistics"}>
                            Statistics
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton onClick={() => onSuccessfulLogout()}>
                            Logout
                        </ListItemButton>
                    </ListItem>
                </List>
            </Sheet>
        )
    } else {
        return (
            <Sheet>
                <List orientation={"horizontal"} variant="outlined"
                      sx={{
                          marginTop: 2,
                          boxShadow: 'sm',
                          borderRadius: 'sm',
                          '--List-radius': '8px',
                          '--List-padding': '4px'
                      }}
                >
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/visitor-quizzes"}>
                            Quizzes
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/statistics"}>
                            Statistics
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/login"}>
                            Login
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton component={RouterLink} to={"/signup"}>
                            Signup
                        </ListItemButton>
                    </ListItem>
                </List>
            </Sheet>
        );
    }
}

export default Appbar;