package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.JourneyController;
import fr.utc.ai13backend.controller.dto.journey.AddJourneyDTO;
import fr.utc.ai13backend.exception.JourneyNotFoundException;
import fr.utc.ai13backend.model.Journey;
import fr.utc.ai13backend.service.JourneyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class JourneyControllerTests {

    private MockMvc mockMvc;

    @Mock
    private JourneyService journeyService;

    @InjectMocks
    private JourneyController journeyController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(journeyController).build();
    }

    @Test
    public void whenAddJourney_thenJourneyIsAdded() throws Exception {
        // Arrange
        AddJourneyDTO addJourneyDTO = new AddJourneyDTO(1, 30, Arrays.asList(1, 2, 3));
        Journey addedJourney = new Journey(); // Populate with test data

        given(journeyService.addJourney(any(Integer.class), any(Integer.class), any(List.class))).willReturn(addedJourney);

        // Act & Assert
        mockMvc.perform(post("/api/v1/journeys")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(addJourneyDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGetJourney_thenReturnsJourney() throws Exception {
        // Arrange
        int journeyId = 1;
        Journey journey = new Journey(); // Populate with test data

        given(journeyService.getJourney(eq(journeyId))).willReturn(journey);

        // Act & Assert
        mockMvc.perform(get("/api/v1/journeys/" + journeyId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenGetCurrentUserJourneys_thenReturnsJourneys() throws Exception {
        // Arrange
        List<Journey> journeys = Arrays.asList(new Journey(), new Journey()); // Populate with test data

        given(journeyService.getCurrentUserJourneys()).willReturn(journeys);

        // Act & Assert
        mockMvc.perform(get("/api/v1/journeys/current-user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
