package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.AnswerController;
import fr.utc.ai13backend.controller.dto.answer.AddAnswerDTO;
import fr.utc.ai13backend.exception.AnswerNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.service.AnswerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AnswerControllerTests {

    private MockMvc mockMvc;

    @Mock
    private AnswerService answerService;

    @InjectMocks
    private AnswerController answerController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(answerController).build();
    }

    @Test
    public void whenGetAnswer_thenReturnsAnswer() throws Exception {
        // Arrange
        int quizId = 1, questionId = 1, answerId = 1;
        Answer answer = new Answer(); // Populate with test data

        given(answerService.getAnswer(eq(answerId), eq(questionId), eq(quizId))).willReturn(answer);

        // Act & Assert
        mockMvc.perform(get("/api/v1/quizzes/" + quizId + "/questions/" + questionId + "/answers/" + answerId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenAddAnswer_thenAnswerIsAdded() throws Exception {
        // Arrange
        int quizId = 1, questionId = 1;
        AddAnswerDTO addAnswerDTO = new AddAnswerDTO("Answer", true, 1);
        Answer addedAnswer = new Answer(); // Populate with test data

        given(answerService.addAnswer(eq(questionId), any(String.class), any(Boolean.class), any(Integer.class), eq(quizId))).willReturn(addedAnswer);

        // Act & Assert
        mockMvc.perform(post("/api/v1/quizzes/" + quizId + "/questions/" + questionId + "/answers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(addAnswerDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteAnswer_thenAnswerIsDeleted() throws Exception {
        // Arrange
        int quizId = 1, questionId = 1, answerId = 1;

        // Act & Assert
        mockMvc.perform(delete("/api/v1/quizzes/" + quizId + "/questions/" + questionId + "/answers/" + answerId))
                .andExpect(status().isOk());
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
