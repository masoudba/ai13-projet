package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.QuizController;
import fr.utc.ai13backend.controller.dto.quiz.AddQuizDTO;
import fr.utc.ai13backend.controller.dto.quiz.UpdateQuizDTO;
import fr.utc.ai13backend.exception.*;
import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.service.QuizService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class QuizControllerTests {

    private MockMvc mockMvc;

    @Mock
    private QuizService quizService;

    @InjectMocks
    private QuizController quizController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(quizController).build();
    }

    @Test
    public void whenGetQuizzes_thenReturnsQuizzes() throws Exception {
        // Arrange
        List<Quiz> quizzes = Arrays.asList(new Quiz(), new Quiz()); // Populate with test data as necessary
        given(quizService.getQuizzes()).willReturn(quizzes);

        // Act & Assert
        mockMvc.perform(get("/api/v1/quizzes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenGetPageOfQuizzes_thenReturnsPage() throws Exception {
        // Arrange
        Page<Quiz> page = new PageImpl<>(Arrays.asList(new Quiz(), new Quiz())); // Populate with test data
        given(quizService.getPageOfQuizzes(any(Integer.class), any(Integer.class))).willReturn(page);

        // Act & Assert
        mockMvc.perform(get("/api/v1/quizzes/page?pageNumber=0&pageSize=2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenAddQuiz_thenQuizIsAdded() throws Exception {
        // Arrange
        AddQuizDTO addQuizDTO = new AddQuizDTO("QuizName", 1, "Description", 2);
        Quiz addedQuiz = new Quiz(); // Populate with test data

        given(quizService.addQuiz(any(String.class), any(Integer.class), any(String.class), any(Integer.class))).willReturn(addedQuiz);

        // Act & Assert
        mockMvc.perform(post("/api/v1/quizzes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(addQuizDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdateQuiz_thenQuizIsUpdated() throws Exception {
        // Arrange
        int quizId = 1;
        UpdateQuizDTO updateQuizDTO = new UpdateQuizDTO(1, "UpdatedName", "UpdatedDescription", 3);
        Quiz updatedQuiz = new Quiz(); // Populate with test data

        given(quizService.updateQuiz(eq(quizId), any(String.class), any(Integer.class), any(String.class), any(Integer.class))).willReturn(updatedQuiz);

        // Act & Assert
        mockMvc.perform(put("/api/v1/quizzes/" + quizId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updateQuizDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenActivateQuiz_thenQuizIsActivated() throws Exception {
        // Arrange
        int quizId = 1;
        Quiz activatedQuiz = new Quiz(); // Populate with test data

        given(quizService.activateQuiz(eq(quizId))).willReturn(activatedQuiz);

        // Act & Assert
        mockMvc.perform(put("/api/v1/quizzes/" + quizId + "/activate"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeactivateQuiz_thenQuizIsDeactivated() throws Exception {
        // Arrange
        int quizId = 1;
        Quiz deactivatedQuiz = new Quiz(); // Populate with test data

        given(quizService.deactivateQuiz(eq(quizId))).willReturn(deactivatedQuiz);

        // Act & Assert
        mockMvc.perform(put("/api/v1/quizzes/" + quizId + "/deactivate"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteQuiz_thenQuizIsDeleted() throws Exception {
        // Arrange
        int quizId = 1;

        // Act & Assert
        mockMvc.perform(delete("/api/v1/quizzes/" + quizId))
                .andExpect(status().isOk());
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}