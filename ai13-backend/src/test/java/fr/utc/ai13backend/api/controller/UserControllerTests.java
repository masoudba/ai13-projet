package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.UserController;
import fr.utc.ai13backend.controller.dto.user.RegisterUserDTO;
import fr.utc.ai13backend.exception.*;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTests {

    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void whenRegisterUser_thenUserIsRegistered() throws Exception {
        // Arrange
        RegisterUserDTO registerUserDTO = new RegisterUserDTO("test@example.com", "password", "Test User", "Test Company", "1234567890");
        User registeredUser = new User(); // Assume this is a populated User object

        given(userService.registerUser(any(), any(), any(), any(), any())).willReturn(registeredUser);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(registerUserDTO)))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    @Test
    public void whenGetCurrentUser_thenReturnsUser() throws Exception {
        // Arrange
        User currentUser = new User(); // Assume this is a populated User object

        given(userService.getCurrentUser()).willReturn(currentUser);

        // Act & Assert
        mockMvc.perform(get("/api/v1/users/current"))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    @Test
    public void whenSendValidationCode_thenSuccess() throws Exception {
        // Arrange
        Integer userId = 1;

        // Act & Assert
        mockMvc.perform(get("/api/v1/users/" + userId + "/code"))
                .andExpect(status().isOk());
        // Verify that the service method was called
    }

    @Test
    public void whenActivateUser_thenUserIsActivated() throws Exception {
        // Arrange
        Integer userId = 1;
        int validationCode = 123456;
        User activatedUser = new User(); // Assume this is a populated User object

        given(userService.activateUser(eq(userId), eq(validationCode))).willReturn(activatedUser);

        // Act & Assert
        mockMvc.perform(put("/api/v1/users/" + userId + "/activate")
                        .param("validationCode", String.valueOf(validationCode)))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    @Test
    public void whenDeactivateUser_thenUserIsDeactivated() throws Exception {
        // Arrange
        Integer userId = 1;
        User deactivatedUser = new User(); // Assume this is a populated User object

        given(userService.deactivateUser(eq(userId))).willReturn(deactivatedUser);

        // Act & Assert
        mockMvc.perform(put("/api/v1/users/" + userId + "/deactivate"))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
