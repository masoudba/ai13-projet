package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.QuestionController;
import fr.utc.ai13backend.controller.dto.question.AddQuestionDTO;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.service.QuestionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class QuestionControllerTests {

    private MockMvc mockMvc;

    @Mock
    private QuestionService questionService;

    @InjectMocks
    private QuestionController questionController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(questionController).build();
    }

    @Test
    public void whenGetQuestions_thenReturnsQuestions() throws Exception {
        // Arrange
        int quizId = 1;
        List<Question> questions = Arrays.asList(new Question(), new Question()); // Populate with test data as necessary
        given(questionService.getQuestions(eq(quizId))).willReturn(questions);

        // Act & Assert
        mockMvc.perform(get("/api/v1/quizzes/" + quizId + "/questions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenAddQuestion_thenQuestionIsAdded() throws Exception {
        // Arrange
        int quizId = 1;
        AddQuestionDTO addQuestionDTO = new AddQuestionDTO("What is 2+2?", "4", Arrays.asList("3", "5"), 2);
        Question addedQuestion = new Question(); // Populate with test data

        given(questionService.addQuestion(eq(quizId), any(String.class), any(List.class), any(String.class), any(Integer.class))).willReturn(addedQuestion);

        // Act & Assert
        mockMvc.perform(post("/api/v1/quizzes/" + quizId + "/questions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(addQuestionDTO)))
                .andExpect(status().isOk());
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
