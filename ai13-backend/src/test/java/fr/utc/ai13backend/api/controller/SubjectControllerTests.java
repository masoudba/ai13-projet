package fr.utc.ai13backend.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.ai13backend.controller.SubjectController;
import fr.utc.ai13backend.controller.dto.subject.AddSubjectDTO;
import fr.utc.ai13backend.controller.dto.subject.UpdateSubjectDTO;
import fr.utc.ai13backend.exception.SubjectNotFoundException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.model.Subject;
import fr.utc.ai13backend.service.SubjectService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SubjectControllerTests {

    private MockMvc mockMvc;

    @Mock
    private SubjectService subjectService;

    @InjectMocks
    private SubjectController subjectController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(subjectController).build();
    }

    @Test
    public void whenGetSubjects_thenReturnsSubjects() throws Exception {
        // Arrange
        List<Subject> subjects = Arrays.asList(new Subject(), new Subject()); // Populate with test data as necessary
        given(subjectService.getSubjects()).willReturn(subjects);

        // Act & Assert
        mockMvc.perform(get("/api/v1/subjects"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        // Add more assertions as necessary
    }

    @Test
    public void whenAddSubject_thenSubjectIsAdded() throws Exception {
        // Arrange
        AddSubjectDTO addSubjectDTO = new AddSubjectDTO("Math", 1);
        Subject addedSubject = new Subject(); // Populate with test data

        given(subjectService.addSubject(any(String.class), any(Integer.class))).willReturn(addedSubject);

        // Act & Assert
        mockMvc.perform(post("/api/v1/subjects")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(addSubjectDTO)))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    @Test
    public void whenUpdateSubject_thenSubjectIsUpdated() throws Exception {
        // Arrange
        int subjectId = 1;
        UpdateSubjectDTO updateSubjectDTO = new UpdateSubjectDTO("Science", 2, true);
        Subject updatedSubject = new Subject(); // Populate with test data

        given(subjectService.updateSubject(eq(subjectId), any(String.class), any(Integer.class), any(Boolean.class))).willReturn(updatedSubject);

        // Act & Assert
        mockMvc.perform(put("/api/v1/subjects/" + subjectId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updateSubjectDTO)))
                .andExpect(status().isOk());
        // Add more assertions as necessary
    }

    @Test
    public void whenDeleteSubject_thenSubjectIsDeleted() throws Exception {
        // Arrange
        int subjectId = 1;

        // Act & Assert
        mockMvc.perform(delete("/api/v1/subjects/" + subjectId))
                .andExpect(status().isOk());
        // Verify that the service method was called
    }

    // Helper method to convert object to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}