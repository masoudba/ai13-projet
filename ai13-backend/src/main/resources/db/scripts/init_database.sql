-- Table pour les rôles
CREATE TABLE Role
(
    name VARCHAR PRIMARY KEY
);

-- Table pour les utilisateurs
CREATE TABLE App_User
(
    id               SERIAL PRIMARY KEY,
    email            VARCHAR UNIQUE NOT NULL,
    password         VARCHAR     NOT NULL,
    name             VARCHAR     NOT NULL,
    company          VARCHAR,
    phone_number     VARCHAR,
    creation_instant TIMESTAMPTZ NOT NULL,
    is_active        BOOLEAN     NOT NULL,
    validation_code  INT,
    role_name        VARCHAR     NOT NULL REFERENCES Role (name)
);

-- Table pour les sujets
CREATE TABLE Subject
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR    NOT NULL,
    "order"   INT UNIQUE NOT NULL,
    is_active BOOLEAN    NOT NULL
);

-- Table pour les questionnaires (Quiz)
CREATE TABLE Quiz
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR                     NOT NULL,
    description VARCHAR,
    subject_id  INT REFERENCES Subject (id) NOT NULL,
    "order"     INT                         NOT NULL,
    is_active   BOOLEAN                     NOT NULL
);

-- Table pour les questions
CREATE TABLE Question
(
    id        SERIAL PRIMARY KEY,
    label     VARCHAR NOT NULL,
    is_active BOOLEAN NOT NULL,
    "order"   INT     NOT NULL,
    quiz_id   INT REFERENCES Quiz (id)
);

-- Table pour les réponses aux questions
CREATE TABLE Answer
(
    id          SERIAL PRIMARY KEY,
    label       VARCHAR                      NOT NULL,
    is_active   BOOLEAN                      NOT NULL,
    "order"     INT                          NOT NULL,
    question_id INT REFERENCES question (id) NOT NULL,
    is_correct  BOOLEAN                      NOT NULL
);

-- Table pour les parcours (Journey)
CREATE TABLE Journey
(
    id       SERIAL PRIMARY KEY,
    quiz_id  INT REFERENCES Quiz (id)     NOT NULL,
    user_id  INT REFERENCES App_User (id) NOT NULL,
    score    INT,
    duration INT
);

-- Table des réponses choisies par l’utilisateur
CREATE TABLE Journey_Answer
(
    journey_id INT REFERENCES Journey (id),
    answer_id  INT REFERENCES Answer (id),
    CONSTRAINT pk_journey_answer PRIMARY KEY (journey_id, answer_id)
);
