package fr.utc.ai13backend.configuration;

import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class JwtUtility {
    private final UserService userService;
    private final int jwtValidityDuration;
    private final String jwtSecretKey;

    public JwtUtility(UserService userService,
                      @Value("${app.security.jwt-validity-duration}") int jwtValidityDuration,
                      @Value("${app.security.jwt-secret-key}") String jwtSecretKey) {
        this.userService = userService;
        this.jwtValidityDuration = jwtValidityDuration;
        this.jwtSecretKey = jwtSecretKey;
    }

    public String generateAccessToken(User user) {
        final long expirationDuration = TimeUnit.MINUTES.toMillis(jwtValidityDuration);
        final Date currentDate = new Date(System.currentTimeMillis());
        final Date expirationDate = new Date(System.currentTimeMillis() + expirationDuration);

        return Jwts.builder()
                .setSubject(String.valueOf(user.getId()))
                .setIssuedAt(currentDate)
                .setExpiration(expirationDate)
                .claim("email", user.getEmail())
                .claim("authorities", user.getAuthorities())
                .claim("name", user.getName())
                .claim("phoneNumber", user.getPhoneNumber())
                .claim("company", user.getCompany())
                .signWith(SignatureAlgorithm.HS256, jwtSecretKey)
                .compact();
    }

    public User getUserFromJWT(String token) throws UserNotFoundException {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecretKey)
                .parseClaimsJws(token)
                .getBody();

        return userService.getUser(Integer.valueOf(claims.getSubject()));
    }
}
