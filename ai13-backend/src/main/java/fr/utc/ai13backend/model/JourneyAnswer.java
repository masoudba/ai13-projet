package fr.utc.ai13backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "journey_answer")
public class JourneyAnswer {
    @JsonIgnore
    @EmbeddedId
    private JourneyAnswerId id;

    @JsonIgnore
    @MapsId("journeyId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "journey_id", nullable = false)
    private Journey journey;

    @MapsId("answerId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "answer_id", nullable = false)
    private Answer answer;
}
