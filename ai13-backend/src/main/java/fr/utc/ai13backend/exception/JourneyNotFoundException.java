package fr.utc.ai13backend.exception;

public class JourneyNotFoundException extends Exception {
    public JourneyNotFoundException(int id) {
        super("Journey not found with id: " + id);
    }
}
