package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class QuizNotFoundException extends Exception {
    public QuizNotFoundException(int id) {
        super("Quiz not found with id: " + id);
    }
}
