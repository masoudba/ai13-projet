package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RoleNotFoundException extends Exception {
    public RoleNotFoundException(String roleName) {
        super("Role not found with name: " + roleName);
    }
}
