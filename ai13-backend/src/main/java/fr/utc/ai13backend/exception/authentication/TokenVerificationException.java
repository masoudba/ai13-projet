package fr.utc.ai13backend.exception.authentication;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TokenVerificationException extends ResponseStatusException {
    public TokenVerificationException() {
        super(HttpStatus.UNAUTHORIZED, "Authentication failure: Token missing, invalid, revoked or expired");
    }
}
