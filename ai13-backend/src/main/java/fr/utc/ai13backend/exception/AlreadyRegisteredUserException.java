package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AlreadyRegisteredUserException extends Exception {
    public AlreadyRegisteredUserException(String email) {
        super("User already registered with email: " + email);
    }
}
