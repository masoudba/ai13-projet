package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SubjectNotFoundException extends Exception {
    public SubjectNotFoundException(int id) {
        super("Subject not found with id: " + id);
    }
}
