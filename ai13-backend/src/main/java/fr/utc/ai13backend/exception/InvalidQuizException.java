package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class InvalidQuizException extends Exception {
    public InvalidQuizException(int id) {
        super("Quiz " + id + " is invalid");
    }
}
