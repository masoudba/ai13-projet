package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AnswerNotFoundException extends Exception {
    public AnswerNotFoundException(int id) {
        super("Answer not found with id: " + id);
    }
}
