package fr.utc.ai13backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NotUniqueOrderException extends Exception {
    public NotUniqueOrderException(String objectTypeName, int order) {
        super("Order (" + order + ") is not unique for " + objectTypeName);
    }
}
