package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.JourneyAnswer;
import fr.utc.ai13backend.model.JourneyAnswerId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JourneyAnswerRepository extends JpaRepository<JourneyAnswer, JourneyAnswerId> {
}
