package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    boolean existsByOrderAndQuestion(Integer order, Question question);

    Optional<Answer> findByIdAndQuestionAndQuestion_Quiz(Integer id, Question question, Quiz quiz);
}
