package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {
    List<Quiz> findBySubject(Subject subject);

    boolean existsByOrderAndSubject(Integer order, Subject subject);
}
