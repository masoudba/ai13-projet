package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {
}