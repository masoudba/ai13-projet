package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    boolean existsByOrder(Integer order);
}
