package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Journey;
import fr.utc.ai13backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JourneyRepository extends JpaRepository<Journey, Integer> {
    List<Journey> findByUser(User user);
}
