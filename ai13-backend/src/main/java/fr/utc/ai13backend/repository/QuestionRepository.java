package fr.utc.ai13backend.repository;

import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface QuestionRepository extends JpaRepository<Question, Integer> {
    List<Question> findByQuiz(Quiz quiz);

    Optional<Question> findByIdAndQuiz(Integer id, Quiz quiz);


    boolean existsByOrderAndQuiz(Integer order, Quiz quiz);
}
