package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.SubjectNotFoundException;
import fr.utc.ai13backend.model.Subject;
import fr.utc.ai13backend.repository.SubjectRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    private final SubjectRepository subjectRepository;

    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public Subject getSubject(int id) throws SubjectNotFoundException {
        return subjectRepository.findById(id)
                .orElseThrow(() -> new SubjectNotFoundException(id));
    }

    public List<Subject> getSubjects() {
        return subjectRepository.findAll();
    }

    public Subject addSubject(String name, int order) throws NotUniqueOrderException {
        Subject subject = new Subject();
        subject.setIsActive(true);
        subject.setName(name);
        if (subjectRepository.existsByOrder(order)) {
            throw new NotUniqueOrderException("Subject", order);
        }
        subject.setOrder(order);
        return subjectRepository.save(subject);
    }

    public void deleteSubject(int id) throws SubjectNotFoundException {
        Subject subject = getSubject(id);
        subjectRepository.delete(subject);
    }

    public Subject updateSubject(int id, String name, int order, boolean isActive) throws SubjectNotFoundException, NotUniqueOrderException {
        Subject subject = getSubject(id);
        subject.setName(name);
        if (subjectRepository.existsByOrder(order)) {
            throw new NotUniqueOrderException("Subject", order);
        }
        subject.setOrder(order);
        subject.setIsActive(isActive);
        return subjectRepository.save(subject);
    }
}
