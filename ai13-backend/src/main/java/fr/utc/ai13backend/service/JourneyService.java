package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.AnswerNotFoundException;
import fr.utc.ai13backend.exception.JourneyNotFoundException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.model.Journey;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.repository.JourneyRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;

@Service
public class JourneyService {
    private final JourneyRepository journeyRepository;
    private final QuizService quizService;
    private final AnswerService answerService;
    private final UserService userService;

    public JourneyService(JourneyRepository journeyRepository, QuizService quizService, AnswerService answerService, UserService userService) {
        this.journeyRepository = journeyRepository;
        this.quizService = quizService;
        this.answerService = answerService;
        this.userService = userService;
    }

    public Journey addJourney(int quizId, int duration, List<Integer> answersId) throws QuizNotFoundException, AnswerNotFoundException {
        Journey journey = new Journey();

        journey.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        journey.setQuiz(quizService.getQuiz(quizId));
        journey.setDuration(duration);
        journey = journeyRepository.save(journey);

        LinkedHashSet<Answer> answers = new LinkedHashSet<>();
        for (Integer answerId : answersId) {
            Answer answer = answerService.getAnswer(answerId);
            answers.add(answer);
        }
        journey.setAnswers(answers);
        journey.setScore((int) answers.stream()
                .filter(Answer::getIsCorrect)
                .count());
        return journeyRepository.save(journey);
    }

    public Journey getJourney(int id) throws JourneyNotFoundException {
        return journeyRepository.findById(id)
                .orElseThrow(() -> new JourneyNotFoundException(id));
    }

    public List<Journey> getCurrentUserJourneys() {
        User user = userService.getCurrentUser();
        return journeyRepository.findByUser(user);
    }
}
