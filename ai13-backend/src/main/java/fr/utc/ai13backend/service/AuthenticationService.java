package fr.utc.ai13backend.service;

import fr.utc.ai13backend.configuration.JwtUtility;
import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.exception.authentication.InvalidLoginCredentialsException;
import fr.utc.ai13backend.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private final JwtUtility jwtUtility;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public AuthenticationService(JwtUtility jwtUtility, UserService userService, PasswordEncoder passwordEncoder) {
        this.jwtUtility = jwtUtility;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public String login(String email, String password) throws InvalidLoginCredentialsException {
        User user;
        try {
            user = userService.getUserByEmail(email);
        } catch (UserNotFoundException e) {
            throw new InvalidLoginCredentialsException();
        }

        String userPassword = user.getPassword();
        if (!passwordEncoder.matches(password, userPassword)) {
            throw new InvalidLoginCredentialsException();
        }

        return jwtUtility.generateAccessToken(user);
    }
}
