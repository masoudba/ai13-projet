package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuestionNotFoundException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.repository.AnswerRepository;
import fr.utc.ai13backend.repository.QuestionRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuestionService {
    private final QuestionRepository questionRepository;
    private final QuizService quizService;
    private final AnswerRepository answerRepository;

    public QuestionService(QuestionRepository questionRepository, QuizService quizService,
                           AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.quizService = quizService;
        this.answerRepository = answerRepository;
    }

    public Question getQuestion(int id, int quizId) throws QuestionNotFoundException, QuizNotFoundException {
        return questionRepository.findByIdAndQuiz(id, quizService.getQuiz(quizId))
                .orElseThrow(() -> new QuestionNotFoundException(id));
    }

    public List<Question> getQuestions(int quizId) throws QuizNotFoundException {
        return questionRepository.findByQuiz(quizService.getQuiz(quizId));
    }

    public Question addQuestion(int quizId, String label, List<String> incorrectAnswersLabel, String correctAnswerLabel, int order) throws QuizNotFoundException, NotUniqueOrderException {
        Question question = new Question();

        Quiz quiz = quizService.getQuiz(quizId);
        question.setQuiz(quiz);

        question.setIsActive(true);
        question.setLabel(label);

        if (questionRepository.existsByOrderAndQuiz(order, quiz)) {
            throw new NotUniqueOrderException("Question", order);
        }
        question.setOrder(order);

        question = questionRepository.save(question);

        Set<Answer> answers = new HashSet<>();
        List<Integer> orders = new ArrayList<>();
        for (int i = 0; i < incorrectAnswersLabel.size() + 1; i++) {
            orders.add(i + 1);
        }
        Collections.shuffle(orders);

        Answer correctAnswer = new Answer();
        correctAnswer.setLabel(correctAnswerLabel);
        correctAnswer.setIsCorrect(true);
        correctAnswer.setQuestion(question);
        correctAnswer.setOrder(orders.get(0));
        answers.add(answerRepository.save(correctAnswer));

        int i = 1;
        for (String incorrectAnswerLabel : incorrectAnswersLabel) {
            Answer incorrectAnswer = new Answer();
            incorrectAnswer.setLabel(incorrectAnswerLabel);
            incorrectAnswer.setQuestion(question);
            incorrectAnswer.setOrder(i++);
            answers.add(answerRepository.save(incorrectAnswer));
        }

        question.setAnswers(answers);
        return questionRepository.save(question);
    }
}
