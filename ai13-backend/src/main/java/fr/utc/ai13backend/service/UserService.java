package fr.utc.ai13backend.service;

import fr.utc.ai13backend.controller.dto.user.GetUserDTO;
import fr.utc.ai13backend.exception.AlreadyRegisteredUserException;
import fr.utc.ai13backend.exception.IncorrectValidationCodeException;
import fr.utc.ai13backend.exception.RoleNotFoundException;
import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Random;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;

    private final Random random = new Random();

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleService roleService, EmailService emailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.emailService = emailService;
    }

    public User registerUser(String email, String password, String name, String company, String phoneNumber) throws RoleNotFoundException, AlreadyRegisteredUserException {
        if (userRepository.existsByEmail(email)) {
            throw new AlreadyRegisteredUserException(email);
        }
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setPassword(passwordEncoder.encode(password));
        user.setName(name);
        user.setCreationInstant(Instant.now());
        user.setCompany(company);
        user.setRole(roleService.getRole("ROLE_INTERN"));
        user.setPhoneNumber(phoneNumber);
        user.setIsActive(false);
        return userRepository.save(user);
    }

    public User activateUser(Integer userId, int validationCode) throws IncorrectValidationCodeException, UserNotFoundException {
        User user = this.getUser(userId);
        if (user.getValidationCode() == validationCode) {
            user.setIsActive(true);
            return userRepository.save(user);
        } else {
            throw new IncorrectValidationCodeException();
        }
    }

    public void sendValidationCode(Integer id) throws UserNotFoundException {
        User user = this.getUser(id);
        int code = generateValidationCode();
        user.setValidationCode(code);
        userRepository.save(user);
        emailService.sendValidationCodeEmail(user.getEmail(), code);
    }

    private int generateValidationCode() {
        return 100000 + random.nextInt(900000);
    }

    public User deactivateUser(int userId) throws UserNotFoundException {
        User user = this.getUser(userId);
        user.setIsActive(false);
        return userRepository.save(user);
    }

    public User getUser(Integer id) throws UserNotFoundException {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    public User getUserByEmail(String email) throws UserNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    public User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public List<GetUserDTO> getAllUsers() {
        return userRepository.findAll().stream()
                .map(user -> new GetUserDTO(user.getId(), user.getName()))
                .toList();
    }
}
