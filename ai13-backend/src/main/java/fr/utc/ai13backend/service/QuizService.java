package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.InvalidQuizException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.exception.SubjectNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.model.Subject;
import fr.utc.ai13backend.repository.QuizRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class QuizService {
    private final QuizRepository quizRepository;
    private final SubjectService subjectService;

    public QuizService(QuizRepository quizRepository, SubjectService subjectService) {
        this.quizRepository = quizRepository;
        this.subjectService = subjectService;
    }

    public Quiz getQuiz(int id) throws QuizNotFoundException {
        return quizRepository.findById(id)
                .orElseThrow(() -> new QuizNotFoundException(id));
    }

    public List<Quiz> getQuizzes() {
        return quizRepository.findAll();
    }

    public Page<Quiz> getPageOfQuizzes(int pageNumber, int pageSize) {
        return quizRepository.findAll(PageRequest.of(pageNumber, pageSize));
    }

    public Quiz addQuiz(String name, int subjectId, String description, int order) throws SubjectNotFoundException, NotUniqueOrderException {
        Quiz quiz = new Quiz();
        quiz.setName(name);

        Subject subject = subjectService.getSubject(subjectId);
        quiz.setSubject(subject);

        quiz.setDescription(description);

        if (quizRepository.existsByOrderAndSubject(order, subject)) {
            throw new NotUniqueOrderException("Quiz", order);
        }
        quiz.setOrder(order);

        quiz.setIsActive(false);
        return quizRepository.save(quiz);
    }

    public void deleteQuiz(int id) throws QuizNotFoundException {
        Quiz quiz = getQuiz(id);
        quizRepository.delete(quiz);
    }

    public Quiz updateQuiz(int id, String name, int subjectId, String description, int order) throws QuizNotFoundException, SubjectNotFoundException, NotUniqueOrderException {
        Quiz quiz = getQuiz(id);
        quiz.setName(name);
        quiz.setDescription(description);

        Subject subject = subjectService.getSubject(subjectId);
        quiz.setSubject(subject);

        if (quizRepository.existsByOrderAndSubject(order, subject)) {
            throw new NotUniqueOrderException("Quiz", order);
        }
        quiz.setOrder(order);

        return quizRepository.save(quiz);
    }

    public Quiz activateQuiz(int id) throws QuizNotFoundException, InvalidQuizException {
        Quiz quiz = getQuiz(id);
        Set<Question> questions = quiz.getQuestions();
        if (!questions.stream().allMatch(this::isQuestionValid)) {
            throw new InvalidQuizException(quiz.getId());
        }
        quiz.setIsActive(true);
        return quizRepository.save(quiz);
    }

    /**
     * Return whether a question is valid. A question is defined as valid
     * if it has at least 2 answers including one (and only one) correct answer,
     * and if each answers has a different order number
     *
     * @param question The question to be tested as valid or not
     * @return whether a question is valid or not
     */
    private boolean isQuestionValid(Question question) {
        return question.getAnswers().size() >= 2
                && question.getAnswers().stream().filter((Answer::getIsCorrect)).count() == 1
                && question.getAnswers().stream().map(Answer::getOrder).distinct().count() == question.getAnswers().size();
    }

    public Quiz deactivateQuiz(int id) throws QuizNotFoundException {
        Quiz quiz = getQuiz(id);
        quiz.setIsActive(false);
        return quizRepository.save(quiz);
    }

    public List<Quiz> getQuizzesOfSubject(int subjectId) throws SubjectNotFoundException {
        Subject subject = subjectService.getSubject(subjectId);
        return quizRepository.findBySubject(subject);
    }
}
