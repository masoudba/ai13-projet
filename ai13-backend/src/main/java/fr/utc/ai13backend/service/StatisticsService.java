package fr.utc.ai13backend.service;

import fr.utc.ai13backend.controller.dto.statistics.GetStatisticsOfUserDTO;
import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.model.Journey;
import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.repository.JourneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StatisticsService {

    private final JourneyRepository journeyRepository;
    private final UserService userService;

    @Autowired
    public StatisticsService(JourneyRepository journeyRepository, UserService userService) {
        this.journeyRepository = journeyRepository;
        this.userService = userService;
    }

    public List<GetStatisticsOfUserDTO> getStatsByQuizForUser(int userId) throws UserNotFoundException {
        User user = userService.getUser(userId);
        List<GetStatisticsOfUserDTO> getStatisticsOfUserDTOS = new ArrayList<>();

        List<Journey> journeys = journeyRepository.findByUser(user);

        Map<Quiz, List<Journey>> collected = journeys.stream()
                .collect(Collectors.groupingBy(Journey::getQuiz));

        for (Map.Entry<Quiz, List<Journey>> entry : collected.entrySet()) {
            List<Journey> journeysOfQuiz = entry.getValue();
            Journey bestJourney = journeysOfQuiz.stream().max(Comparator.comparingInt(Journey::getScore))
                    .orElse(null);
            if (bestJourney == null) {
                continue;
            }
            getStatisticsOfUserDTOS.add(new GetStatisticsOfUserDTO(entry.getKey(), bestJourney.getScore(), bestJourney.getDuration()));
        }

        return getStatisticsOfUserDTOS;
    }

    // Implémentation de getStatsByThemeForUser
    public Map<String, Map<String, Double>> getStatsByThemeForUser(User user) {
        List<Journey> journeys = journeyRepository.findByUser(user);
        return journeys.stream()
                .collect(Collectors.groupingBy(journey -> journey.getQuiz().getSubject().getName(),
                        Collectors.averagingInt(Journey::getScore)))
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> {
                            Map<String, Double> stats = new HashMap<>();
                            stats.put("averageDuration", entry.getValue());
                            double averageScore = journeys.stream()
                                    .filter(journey -> journey.getQuiz().getSubject().getName().equals(entry.getKey()))
                                    .collect(Collectors.averagingInt(Journey::getScore));
                            stats.put("averageScore", averageScore);
                            return stats;
                        }));
    }

    // Méthode pour obtenir le classement des stagiaires par questionnaire
    public Map<String, List<Map.Entry<Integer, Integer>>> getRankingByQuiz() {
        List<Journey> allJourneys = journeyRepository.findAll();
        Map<String, List<Map.Entry<Integer, Integer>>> ranking = new HashMap<>();

        allJourneys.stream()
                .collect(Collectors.groupingBy(journey -> journey.getQuiz().getName()))
                .forEach((quizName, journeys) -> {
                    Map<Integer, Integer> bestScores = journeys.stream()
                            .collect(Collectors.toMap(
                                    journey -> journey.getUser().getId(),
                                    Journey::getScore,
                                    Integer::max)); // Prendre le meilleur score pour chaque utilisateur
                    List<Map.Entry<Integer, Integer>> sortedScores = new ArrayList<>(bestScores.entrySet());
                    sortedScores.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
                    ranking.put(quizName, sortedScores);
                });

        return ranking;
    }

    public Map<String, List<Map.Entry<Integer, Integer>>> getRankingByTheme() {
        List<Journey> allJourneys = journeyRepository.findAll();
        Map<String, List<Map.Entry<Integer, Integer>>> rankingByTheme = new HashMap<>();

        allJourneys.stream()
                .collect(Collectors.groupingBy(journey -> journey.getQuiz().getSubject().getName()))
                .forEach((themeName, journeys) -> {
                    Map<Integer, Integer> bestScoresPerUser = journeys.stream()
                            .collect(Collectors.toMap(
                                    journey -> journey.getUser().getId(),
                                    Journey::getScore,
                                    Integer::max)); // Utiliser le meilleur score pour chaque utilisateur
                    List<Map.Entry<Integer, Integer>> sortedScores = new ArrayList<>(bestScoresPerUser.entrySet());
                    sortedScores.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
                    rankingByTheme.put(themeName, sortedScores);
                });

        return rankingByTheme;
    }
}
