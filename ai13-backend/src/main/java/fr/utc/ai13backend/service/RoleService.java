package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.RoleNotFoundException;
import fr.utc.ai13backend.model.Role;
import fr.utc.ai13backend.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role getRole(String roleName) throws RoleNotFoundException {
        return roleRepository.findById(roleName)
                .orElseThrow(() -> new RoleNotFoundException(roleName));
    }
}
