package fr.utc.ai13backend.service;

import fr.utc.ai13backend.exception.AnswerNotFoundException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuestionNotFoundException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.repository.AnswerRepository;
import org.springframework.stereotype.Service;

@Service
public class AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionService questionService;
    private final QuizService quizService;

    public AnswerService(AnswerRepository answerRepository, QuestionService questionService, QuizService quizService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.quizService = quizService;
    }

    public Answer getAnswer(int id) throws AnswerNotFoundException {
        return answerRepository.findById(id)
                .orElseThrow(() -> new AnswerNotFoundException(id));
    }

    public Answer getAnswer(int id, int questionId, int quizId) throws AnswerNotFoundException, QuestionNotFoundException, QuizNotFoundException {
        return answerRepository.findByIdAndQuestionAndQuestion_Quiz(
                        id,
                        questionService.getQuestion(questionId, quizId),
                        quizService.getQuiz(quizId))
                .orElseThrow(() -> new AnswerNotFoundException(id));
    }

    public Answer addAnswer(int questionId, String label, boolean isCorrect, int order, int quizId) throws QuestionNotFoundException, NotUniqueOrderException, QuizNotFoundException {
        Answer answer = new Answer();
        Question question = questionService.getQuestion(questionId, quizId);
        answer.setQuestion(question);
        answer.setLabel(label);
        answer.setIsCorrect(isCorrect);
        if (answerRepository.existsByOrderAndQuestion(order, question)) {
            throw new NotUniqueOrderException("Question", order);
        }
        answer.setOrder(order);
        answer.setIsActive(true);
        return answerRepository.save(answer);
    }

    public void deleteAnswer(int id, int questionId, int quizId)
            throws AnswerNotFoundException, QuestionNotFoundException, QuizNotFoundException {
        answerRepository.delete(this.getAnswer(id, questionId, quizId));
    }
}
