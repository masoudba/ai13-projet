package fr.utc.ai13backend.controller.dto.journey;

import jakarta.validation.constraints.NotNull;

import java.util.List;

public record AddJourneyDTO(
        @NotNull int quizId,
        @NotNull int duration,
        @NotNull List<Integer> answersId
) {
}
