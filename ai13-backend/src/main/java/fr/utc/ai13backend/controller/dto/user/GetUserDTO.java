package fr.utc.ai13backend.controller.dto.user;

public record GetUserDTO(
        int id,
        String name
) {
}