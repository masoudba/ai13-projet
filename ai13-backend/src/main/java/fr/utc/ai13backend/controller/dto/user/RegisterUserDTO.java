package fr.utc.ai13backend.controller.dto.user;

import jakarta.validation.constraints.NotBlank;

public record RegisterUserDTO(
        @NotBlank
        String email,
        @NotBlank
        String password,
        @NotBlank
        String name,
        String company,
        String phoneNumber
) {
}
