package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.authentication.LoginRequestDTO;
import fr.utc.ai13backend.controller.dto.authentication.SuccessLoginResponseDTO;
import fr.utc.ai13backend.exception.authentication.InvalidLoginCredentialsException;
import fr.utc.ai13backend.service.AuthenticationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/authentication")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping(value = "/login")
    public SuccessLoginResponseDTO login(@Valid @RequestBody LoginRequestDTO loginRequestDTO) throws InvalidLoginCredentialsException {
        String token = authenticationService.login(loginRequestDTO.email(), loginRequestDTO.password());
        return new SuccessLoginResponseDTO(token);
    }
}
