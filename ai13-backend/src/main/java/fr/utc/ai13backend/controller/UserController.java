package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.user.GetUserDTO;
import fr.utc.ai13backend.controller.dto.user.RegisterUserDTO;
import fr.utc.ai13backend.exception.AlreadyRegisteredUserException;
import fr.utc.ai13backend.exception.IncorrectValidationCodeException;
import fr.utc.ai13backend.exception.RoleNotFoundException;
import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.service.UserService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<GetUserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping
    public User registerUser(@RequestBody @Valid RegisterUserDTO registerUserDTO)
            throws AlreadyRegisteredUserException, RoleNotFoundException {
        return userService.registerUser(
                registerUserDTO.email(),
                registerUserDTO.password(),
                registerUserDTO.name(),
                registerUserDTO.company(),
                registerUserDTO.phoneNumber());
    }

    @GetMapping("/current")
    public User getCurrentUser() {
        return userService.getCurrentUser();
    }

    @GetMapping("/{userId}/code")
    public void sendValidationCode(@PathVariable Integer userId) throws UserNotFoundException {
        userService.sendValidationCode(userId);
    }

    @PutMapping("/{userId}/activate")
    public User activateUser(@PathVariable Integer userId, @RequestParam int validationCode)
            throws UserNotFoundException, IncorrectValidationCodeException {
        return userService.activateUser(userId, validationCode);
    }

    @PutMapping("/{userId}/deactivate")
    @RolesAllowed("ADMIN")
    public User deactivateUser(@PathVariable Integer userId) throws UserNotFoundException {
        return userService.deactivateUser(userId);
    }
}
