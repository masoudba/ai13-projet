package fr.utc.ai13backend.controller.dto.statistics;

import fr.utc.ai13backend.model.Quiz;

public record GetStatisticsOfUserDTO(
        Quiz quiz,
        int bestScore,
        int duration
) {
}
