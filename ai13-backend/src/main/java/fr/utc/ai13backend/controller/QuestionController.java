package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.question.AddQuestionDTO;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Question;
import fr.utc.ai13backend.service.QuestionService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/quizzes/{quizId}/questions")
public class QuestionController {
    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping
    public List<Question> getQuestions(@PathVariable int quizId) throws QuizNotFoundException {
        return questionService.getQuestions(quizId);
    }

    @PostMapping
    @RolesAllowed("ADMIN")
    public Question addQuestion(@PathVariable int quizId, @RequestBody @Valid AddQuestionDTO addQuestionDTO) throws QuizNotFoundException, NotUniqueOrderException {
        return questionService.addQuestion(quizId,
                addQuestionDTO.label(),
                addQuestionDTO.incorrectAnswersLabel(),
                addQuestionDTO.correctAnswerLabel(),
                addQuestionDTO.order());
    }
}
