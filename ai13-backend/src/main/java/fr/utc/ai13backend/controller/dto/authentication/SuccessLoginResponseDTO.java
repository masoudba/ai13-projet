package fr.utc.ai13backend.controller.dto.authentication;

public record SuccessLoginResponseDTO(String accessToken) {
}
