package fr.utc.ai13backend.controller.dto.question;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public record AddQuestionDTO(
        @NotBlank String label,
        @NotBlank String correctAnswerLabel,
        @NotNull List<String> incorrectAnswersLabel,
        @NotNull int order
) {
}
