package fr.utc.ai13backend.controller.dto.subject;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record AddSubjectDTO(@NotBlank String name, @NotNull int order) {
}
