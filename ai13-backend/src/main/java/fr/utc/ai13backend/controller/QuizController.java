package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.quiz.AddQuizDTO;
import fr.utc.ai13backend.controller.dto.quiz.PartialInfosOfQuizDTO;
import fr.utc.ai13backend.controller.dto.quiz.UpdateQuizDTO;
import fr.utc.ai13backend.exception.InvalidQuizException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.exception.SubjectNotFoundException;
import fr.utc.ai13backend.model.Quiz;
import fr.utc.ai13backend.service.QuizService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class QuizController {
    private final QuizService quizService;

    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping("/api/v1/quizzes")
    public List<Quiz> getQuizzes() {
        return quizService.getQuizzes();
    }

    @GetMapping("/api/v1/subjects/{subjectId}/quizzes/partial-infos")
    public List<PartialInfosOfQuizDTO> getPartialInfosOfQuizzesOfSubject(@PathVariable int subjectId) throws SubjectNotFoundException {
        List<Quiz> quizzes = quizService.getQuizzesOfSubject(subjectId);

        List<PartialInfosOfQuizDTO> partialInfosOfQuizzes = new ArrayList<>(quizzes.size());
        for (Quiz quiz : quizzes) {
            partialInfosOfQuizzes.add(new PartialInfosOfQuizDTO(
                    quiz.getId(),
                    quiz.getName(),
                    quiz.getDescription(),
                    quiz.getQuestions().size()
            ));
        }

        return partialInfosOfQuizzes;
    }

    @GetMapping(value = "/api/v1/quizzes/page")
    public Page<Quiz> getPageOfQuizzes(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return quizService.getPageOfQuizzes(pageNumber, pageSize);
    }

    @GetMapping("/api/v1/subjects/{subjectId}/quizzes")
    public List<Quiz> getQuizzesOfSubject(@PathVariable int subjectId) throws SubjectNotFoundException {
        return quizService.getQuizzesOfSubject(subjectId);
    }

    @PostMapping("/api/v1/quizzes")
    @RolesAllowed("ADMIN")
    public Quiz addQuiz(@RequestBody @Valid AddQuizDTO addQuizDTO) throws SubjectNotFoundException, NotUniqueOrderException {
        return quizService.addQuiz(addQuizDTO.name(),
                addQuizDTO.subjectId(),
                addQuizDTO.description(),
                addQuizDTO.order());
    }

    @PutMapping("/api/v1/quizzes/{quizId}")
    @RolesAllowed("ADMIN")
    public Quiz updateQuiz(@PathVariable int quizId, @RequestBody @Valid UpdateQuizDTO updateQuizDTO) throws QuizNotFoundException, SubjectNotFoundException, NotUniqueOrderException {
        return quizService.updateQuiz(quizId,
                updateQuizDTO.name(),
                updateQuizDTO.subjectId(),
                updateQuizDTO.description(),
                updateQuizDTO.order());
    }

    @PutMapping("/api/v1/quizzes/{quizId}/activate")
    @RolesAllowed("ADMIN")
    public Quiz activateQuiz(@PathVariable int quizId) throws QuizNotFoundException, InvalidQuizException {
        return quizService.activateQuiz(quizId);
    }

    @PutMapping("/api/v1/quizzes/{quizId}/deactivate")
    @RolesAllowed("ADMIN")
    public Quiz deactivateQuiz(@PathVariable int quizId) throws QuizNotFoundException {
        return quizService.deactivateQuiz(quizId);
    }

    @DeleteMapping("/api/v1/quizzes/{quizId}")
    @RolesAllowed("ADMIN")
    public void deleteQuiz(@PathVariable int quizId) throws QuizNotFoundException {
        quizService.deleteQuiz(quizId);
    }
}
