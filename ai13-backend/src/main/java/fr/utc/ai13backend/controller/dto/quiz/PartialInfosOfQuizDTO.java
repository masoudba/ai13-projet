package fr.utc.ai13backend.controller.dto.quiz;

public record PartialInfosOfQuizDTO(int id, String name, String description, int numberOfQuestions) {
}
