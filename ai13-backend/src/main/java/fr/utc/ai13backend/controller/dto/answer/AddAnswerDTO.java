package fr.utc.ai13backend.controller.dto.answer;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record AddAnswerDTO(
        @NotBlank String label,
        @NotNull boolean isCorrect,
        @NotNull int order
) {
}
