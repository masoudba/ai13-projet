package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.statistics.GetStatisticsOfUserDTO;
import fr.utc.ai13backend.exception.UserNotFoundException;
import fr.utc.ai13backend.model.User;
import fr.utc.ai13backend.service.StatisticsService;
import fr.utc.ai13backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class StatisticsController {

    private final StatisticsService statisticsService;
    private final UserService userService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService, UserService userService) {
        this.statisticsService = statisticsService;
        this.userService = userService;
    }

    @GetMapping("/users/{userId}/statistics")
    public List<GetStatisticsOfUserDTO> getStatisticsOfUser(@PathVariable int userId) throws UserNotFoundException {
        return statisticsService.getStatsByQuizForUser(userId);
    }

    @GetMapping("/statsByTheme/{userId}")
    public Map<String, Map<String, Double>> getStatsByThemeForUser(@PathVariable int userId) throws UserNotFoundException {
        User user = userService.getUser(userId);
        return statisticsService.getStatsByThemeForUser(user);
    }

    @GetMapping("/ranking-by-quiz")
    public Map<String, List<Map.Entry<Integer, Integer>>> getRankingByQuiz() {
        return statisticsService.getRankingByQuiz();
    }

    @GetMapping("/ranking-by-theme")
    public Map<String, List<Map.Entry<Integer, Integer>>> getRankingByTheme() {
        return statisticsService.getRankingByTheme();
    }
}
