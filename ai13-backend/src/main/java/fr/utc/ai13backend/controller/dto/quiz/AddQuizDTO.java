package fr.utc.ai13backend.controller.dto.quiz;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record AddQuizDTO(
        @NotBlank String name,
        @NotNull int subjectId,
        String description,
        @NotNull int order
) {
}
