package fr.utc.ai13backend.controller.dto.quiz;

import jakarta.validation.constraints.NotNull;

public record UpdateQuizDTO(
        @NotNull int subjectId,
        @NotNull String name,
        String description,
        int order
) {
}
