package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.journey.AddJourneyDTO;
import fr.utc.ai13backend.exception.AnswerNotFoundException;
import fr.utc.ai13backend.exception.JourneyNotFoundException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Journey;
import fr.utc.ai13backend.service.JourneyService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/journeys")
public class JourneyController {
    private final JourneyService journeyService;

    public JourneyController(JourneyService journeyService) {
        this.journeyService = journeyService;
    }

    @PostMapping
    public Journey addJourney(@RequestBody @Valid AddJourneyDTO addJourneyDTO) throws AnswerNotFoundException, QuizNotFoundException {
        return journeyService.addJourney(addJourneyDTO.quizId(), addJourneyDTO.duration(), addJourneyDTO.answersId());
    }

    @GetMapping("/{journeyId}")
    public Journey getJourney(@PathVariable int journeyId) throws JourneyNotFoundException {
        return journeyService.getJourney(journeyId);
    }

    @GetMapping("/current-user")
    public List<Journey> getCurrentUserJourneys() {
        return journeyService.getCurrentUserJourneys();
    }
}
