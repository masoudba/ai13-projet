package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.subject.AddSubjectDTO;
import fr.utc.ai13backend.controller.dto.subject.UpdateSubjectDTO;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.SubjectNotFoundException;
import fr.utc.ai13backend.model.Subject;
import fr.utc.ai13backend.service.SubjectService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/subjects")
public class SubjectController {
    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping
    public List<Subject> getSubjects() {
        return subjectService.getSubjects();
    }

    @PostMapping
    @RolesAllowed("ADMIN")
    public Subject addSubject(@RequestBody @Valid AddSubjectDTO addSubjectDTO) throws NotUniqueOrderException {
        return subjectService.addSubject(addSubjectDTO.name(), addSubjectDTO.order());
    }

    @PutMapping("/{subjectId}")
    @RolesAllowed("ADMIN")
    public Subject updateSubject(@PathVariable int subjectId, @RequestBody @Valid UpdateSubjectDTO updateSubjectDTO) throws SubjectNotFoundException, NotUniqueOrderException {
        return subjectService.updateSubject(subjectId,
                updateSubjectDTO.name(),
                updateSubjectDTO.order(),
                updateSubjectDTO.isActive());
    }

    @DeleteMapping("/{subjectId}")
    @RolesAllowed("ADMIN")
    public void deleteSubject(@PathVariable int subjectId) throws SubjectNotFoundException {
        subjectService.deleteSubject(subjectId);
    }
}
