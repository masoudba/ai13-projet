package fr.utc.ai13backend.controller.dto.subject;

import jakarta.validation.constraints.NotBlank;

public record UpdateSubjectDTO(
        @NotBlank String name,
        int order,
        boolean isActive
) {
}
