package fr.utc.ai13backend.controller;

import fr.utc.ai13backend.controller.dto.answer.AddAnswerDTO;
import fr.utc.ai13backend.exception.AnswerNotFoundException;
import fr.utc.ai13backend.exception.NotUniqueOrderException;
import fr.utc.ai13backend.exception.QuestionNotFoundException;
import fr.utc.ai13backend.exception.QuizNotFoundException;
import fr.utc.ai13backend.model.Answer;
import fr.utc.ai13backend.service.AnswerService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/quizzes/{quizId}/questions/{questionId}/answers")
public class AnswerController {
    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/{answerId}")
    public Answer getAnswer(@PathVariable int answerId, @PathVariable int questionId, @PathVariable int quizId)
            throws AnswerNotFoundException, QuestionNotFoundException, QuizNotFoundException {
        return answerService.getAnswer(answerId, questionId, quizId);
    }

    @PostMapping
    @RolesAllowed("ADMIN")
    public Answer addAnswer(@PathVariable int questionId, @PathVariable int quizId, @RequestBody @Valid AddAnswerDTO addAnswerDTO)
            throws NotUniqueOrderException, QuestionNotFoundException, QuizNotFoundException {
        return answerService.addAnswer(questionId, addAnswerDTO.label(), addAnswerDTO.isCorrect(), addAnswerDTO.order(), quizId);
    }

    @DeleteMapping("/{answerId}")
    @RolesAllowed("ADMIN")
    public void deleteAnswer(@PathVariable int answerId, @PathVariable int questionId, @PathVariable int quizId)
            throws AnswerNotFoundException, QuestionNotFoundException, QuizNotFoundException {
        answerService.deleteAnswer(answerId, questionId, quizId);
    }
}
