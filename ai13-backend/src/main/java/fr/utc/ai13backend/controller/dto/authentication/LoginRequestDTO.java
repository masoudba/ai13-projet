package fr.utc.ai13backend.controller.dto.authentication;

import jakarta.validation.constraints.NotBlank;

public record LoginRequestDTO(@NotBlank String email, @NotBlank String password) {
}
